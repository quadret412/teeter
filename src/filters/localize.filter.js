import store from '@/store'
import en from '@/locales/en.json'
import ru from '@/locales/ru.json'

const locales = {
  'ru-RU': ru,
  'en-US': en
}

export default function localizeFilter (key, vars = null) {
  if (!key) return ''
  const locale = store.state.localisation || 'en-US'
  let result
  if (locales[locale][key] === undefined) {
    result = locales.original[key]
  } else {
    result = locales[locale][key]
  }

  if (vars === null) {
    return result || `[Localize error]: key ${key} not found`
  }

  if (result !== undefined) {
    Object.keys(vars).forEach((el) => {
      result = result.replace(el, vars[el])
    })
    return result
  }

  return `[Localize error]: key ${key} not found`
}
