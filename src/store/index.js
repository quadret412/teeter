import Vue from 'vue'
import Vuex from 'vuex'
import {
  MAX_ANGLE,
  MIN_ANGLE,
  MAX_MOMENTUM_DIFFERENCE,
  FALLING_BLOCKS_COUNT
} from '@/constants'
import { generateBlock } from '@/plugins/generateBlock'

Vue.use(Vuex)

const totalBlocksMomentum = (array) => {
  return array.reduce((acc, item) => {
    acc += item.weight * item.offset
    return acc
  }, 0)
}
const calculateIndividualBlockMomentum = (block, isRightBlock = true) => {
  return isRightBlock ? block.weight * block.offset : block.weight * 3
}
const getRandomNumber = (num) => {
  const max = num + 2
  const min = num < 3 ? 1 : num - 2
  return (Math.floor(Math.random() * (max - min + 1)) + min)
}

export default new Vuex.Store({
  state: {
    isPaused: true,
    leftSideBlocks: [],
    rightSideBlocks: [],
    fallingBlocks: [],
    gameOver: false,
    localisation: 'en-US'
  },
  getters: {
    getLeftMomentum (state) {
      return totalBlocksMomentum(state.leftSideBlocks)
    },
    getRightMomentum (state) {
      return totalBlocksMomentum(state.rightSideBlocks)
    },
    getTeeterAngle (state, getters) {
      const { getLeftMomentum, getRightMomentum } = getters

      if (!getLeftMomentum) return MAX_ANGLE
      if (getLeftMomentum === getRightMomentum) return 0
      return getLeftMomentum > getRightMomentum
        ? Math.abs((getLeftMomentum - getRightMomentum) / getLeftMomentum * -100)
        : Math.abs((getRightMomentum - getLeftMomentum) / getRightMomentum * 100)
    },
    gameOverStatus (state, getters) {
      const { getLeftMomentum, getRightMomentum, getTeeterAngle } = getters
      return (
        getTeeterAngle > MAX_ANGLE ||
        getTeeterAngle < MIN_ANGLE ||
        Math.abs(getLeftMomentum - getRightMomentum) > MAX_MOMENTUM_DIFFERENCE
      )
    }
  },
  mutations: {
    setGameOver (state, value) {
      state.gameOver = value
      state.isPaused = true
    },
    setLocalisation (state, locale) {
      state.localisation = locale
    },
    togglePause (state) {
      state.isPaused = !state.isPaused
    },
    addRightBlock (state, preGeneratedBlock) {
      state.rightSideBlocks.push(preGeneratedBlock)
    },
    addLeftBlock (state) {
      const block = state.fallingBlocks.shift()
      state.leftSideBlocks.push(block)
    },
    startCreationOfFallingBlock (state) {
      for (let i = 0; i < FALLING_BLOCKS_COUNT; i++) {
        const newRandomBlock = generateBlock()
        state.fallingBlocks.push(newRandomBlock)
      }
    },
    addFallingBlock (state) {
      const newRandomBlock = generateBlock()
      state.fallingBlocks.push(newRandomBlock)
    },
    moveToTheRight (state) {
      if (state.isPaused || state.fallingBlocks[0].offset === 1) return
      state.fallingBlocks[0].offset -= 1
    },
    moveToTheLeft (state) {
      if (state.isPaused || state.fallingBlocks[0].offset >= 5) return
      state.fallingBlocks[0].offset += 1
    },
    setStateDefault (state) {
      state.isPaused = true
      state.leftSideBlocks = []
      state.rightSideBlocks = []
      state.gameOver = false
      state.fallingBlocks = []
    }
  },
  actions: {
    preGenerateRightBlock ({ state, getters, commit }, isInitial = false) {
      let newRandomBlock = generateBlock()
      if (!isInitial) {
        const maxDiff = Math.abs(getters.getRightMomentum - (getters.getLeftMomentum + calculateIndividualBlockMomentum(state.fallingBlocks[0], false)))
        do {
          newRandomBlock = generateBlock()
        } while (calculateIndividualBlockMomentum(newRandomBlock) !== getRandomNumber(maxDiff))
      }
      commit('addRightBlock', newRandomBlock)
    },
    restartGame ({ commit, dispatch }) {
      commit('setStateDefault')
      commit('startCreationOfFallingBlock')
      dispatch('preGenerateRightBlock', true)
    },
    checkIfGameOver ({ commit, state, getters, dispatch }) {
      commit('addLeftBlock')

      setTimeout(() => {
        if (getters.gameOverStatus) {
          setTimeout(() => {
            commit('setGameOver', true)
          }, 0)
        } else {
          console.warn('1')
          commit('addFallingBlock')
          if (state.leftSideBlocks.length && state.leftSideBlocks.length + 1 !== state.rightSideBlocks.length) {
            console.warn('2')
            dispatch('preGenerateRightBlock')
          }
        }
      }, 250)
    },
    startGame ({ commit, dispatch }) {
      commit('setGameOver', false)
      commit('setStateDefault')
      dispatch('preGenerateRightBlock', true)
      commit('startCreationOfFallingBlock')
    }
  }
})
