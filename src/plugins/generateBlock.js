import { MIN_WEIGHT, MAX_WEIGHT, TEETER_TOTTER_WIDTH, SHAPE_COUNT } from '@/constants'
import { uniqueId } from '@/plugins/generateRandomId'

export function generateBlock () {
  const id = uniqueId()
  const type = Math.floor(Math.random() * SHAPE_COUNT)
  const weight = Math.floor(Math.random() * MAX_WEIGHT) + MIN_WEIGHT
  const offset = Math.floor(Math.random() * TEETER_TOTTER_WIDTH / 2) + 1
  const height = weight * 8

  return {
    id,
    type,
    weight,
    offset,
    height
  }
}
