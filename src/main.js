import Vue from 'vue'
import App from './App.vue'
import store from './store'
import localizeFilter from '@/filters/localize.filter.js'

Vue.config.productionTip = false
Vue.filter('localize', localizeFilter)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
